# Módulo de cliente do Keycloack

from settings import DEBUG_USER_USERNAME, DEBUG_USER_PASSWORD, KEYCLOAK_ADMIN_USERNAME, KEYCLOAK_ADMIN_PASSWORD, KEYCLOAK_API_SECRET, KEYCLOAK_DEFAULT_USER_PASSWORD, KEYCLOAK_BASE_URL, REALM_CLIENT_ID, REALM_CLIENT_SECRET, TLS_VERIFY_CERTIFICATE
from urllib import request, parse
from rauth import OAuth2Service
import json
import ssl

import logging
logging.basicConfig(level=logging.DEBUG)

class Keycloak:
    """
    Biblioteca de funções que interagem com o Keycloack por requisições HTTP
    """

    def __init__(self, keycloak_endpoint, realm=None):
        """
        Constructor method
        """

        # Configurações do Keycloak
        self.keycloak_endpoint = keycloak_endpoint
        self.uri = self.keycloak_endpoint

        # Serviço de geração de token OAuth2.0
        self.oauth_service = OAuth2Service(
            name='service',
            client_id=REALM_CLIENT_ID,
            client_secret=REALM_CLIENT_SECRET,
            access_token_url=f'{self.uri}/auth/realms/{realm}/protocol/openid-connect/token',
            authorize_url=f'{self.uri}/auth/realms/{realm}/protocol/openid-connect/auth',
            base_url='{self.uri}'
        )

        # Contexto SSL a ser utilizado para as conexões HTTP
        self.ssl_context = ssl.create_default_context()
        self.ssl_context.check_hostname = False
        if not TLS_VERIFY_CERTIFICATE:
            self.ssl_context.verify_mode = ssl.CERT_NONE

    def _convert_dict_to_url_query_params(self, dictionary : dict) -> str:
        param_list = [f'{el[0]}={el[1]}' for el in dictionary.items()] 
        param_string = '&'.join(param_list)
        return param_string
 

    def get_admin_token(self) -> str:
        """
        Busca o token de admin no Keycloak

        :returns str: Token de admin do Keycloak
        :raises Exception: Exceção caso não seja possível retornar o token
        """

        route_data = {
            'username': KEYCLOAK_ADMIN_USERNAME,
            'password': KEYCLOAK_ADMIN_PASSWORD,
            'grant_type': 'password',
            'client_id': 'admin-cli',
            'client_secret': KEYCLOAK_API_SECRET
        }
        admin_token_route = f'{self.uri}/auth/realms/master/protocol/openid-connect/token'
        data = parse.urlencode(route_data).encode()
        req = request.Request(admin_token_route, data=data)
        req.add_header('Content-Type', 'application/x-www-form-urlencoded')
        with request.urlopen(req, context=self.ssl_context) as response:
            response_data = json.loads(response.read().decode('utf-8'))
            if response_data:
                return response_data['access_token']
            else:
                raise Exception("Could not get Keycloak admin access token")

    def create_user(self, user: dict, realm: str) -> bool:
        """
        Cria usuário em um realm existente no keycloak

        :param user: Dicionário com informações de usuário recebidas da API
        :type user: dict
        :param realm: Nome do realm onde será feita a criação de usuário
        :type realm: str
        :returns bool: Caso o usuário tenha sido criado retorna True
        :raises Exception: Quaisuer erros lançados pelo Keycloak
        """

        try:
            keycloak_user_representation = {
                "username": user.username,
                "email": user.email,
                "firstName": user.first_name,
                "lastName": user.last_name,
                "enabled": True,
                "emailVerified": True,
                "credentials": [
                    {
                        "type": "password",
                        "value": KEYCLOAK_DEFAULT_USER_PASSWORD,
                        "temporary": True
                    }
                ]
            }

            admin_token = self.get_admin_token()
            user_route = f'{self.uri}/auth/admin/realms/{realm}/users'
            data = json.dumps(keycloak_user_representation).encode('utf-8')
            req = request.Request(user_route, data=data)
            req.add_header('Authorization', f'Bearer {admin_token}')
            req.add_header('Content-Type', 'application/json')
            with request.urlopen(req, context=self.ssl_context) as response:
                response_data = response.read().decode('utf-8')
                if not response_data:
                    return True
                else:
                    raise Exception("User could not be created")
                return response
        except Exception as e:
            raise Exception(e)

    def generate_oauth2_authorization_url(self, realm: str, redirect_uri: str) -> str:
        """
        Retorna a url de autorização

        :param realm: Nome do realm
        :type realm: str
        :param redirect_uri: URL de redirecionamento após autenticação com o Keycloak
        :type redirect_uri: str
        :returns: URL de login no Keycloak
        :raises Exception: Quaisquer erros lançados pelo rauth
        """

        try:
            params = {
                'redirect_uri': redirect_uri,
                'response_type': 'code',
            }
            url = self.oauth_service.get_authorize_url(**params)
            return url
        except Exception:
            raise('Could not retrieve OAuth2.0 endpoint')

    def generate_automatic_oauth2_bearer_token(self, realm: str) -> str:
        """
        Gera um bearer token de forma automatica com um usuário de debug
        :param realm: Nome do realm
        :type realm: str
        """
        keycloak_token_request_body = {
            'grant_type': 'password',
            'client_id': REALM_CLIENT_ID,
            'client_secret': REALM_CLIENT_SECRET,
            'username': DEBUG_USER_USERNAME,
            'password': DEBUG_USER_PASSWORD
        }
        
        generate_token_route = f'{self.uri}/auth/realms/{realm}/protocol/openid-connect/token'
        req = request.Request(generate_token_route, data=parse.urlencode(keycloak_token_request_body).encode('utf-8'))

        with request.urlopen(req, context=self.ssl_context) as response:
            response_data = response.read().decode('utf-8')
            if response_data:
                json_data = json.loads(response_data)
                return f"Bearer {json_data.get('access_token')}"
            else:
                raise Exception("Failed to get token")


    def get_token(self, realm: str, authentication_code: str, redirect_uri: str) -> str:
        """
        Verfica a validade do código junto ao Keycloak e gera um token OAuth2.0

        :param realm: Nome do realm
        :type realm: str
        :param authentication_code: Código de autenticação gerado pelo browser
        :type authentication_code: str
        :param redirect_uri: URL de redirecionamento após autenticação com o Keycloak
        :type redirect_uri: str
        :returns: Token de autorização
        :raises Exception: Quaisquer erros gerados pelo rauth
        """

        try:
            params = {
                'redirect_uri': redirect_uri,
                'grant_type': 'authorization_code',
                'code': authentication_code
            }

            session = self.oauth_service.get_auth_session(data=params, decoder=json.loads, verify=TLS_VERIFY_CERTIFICATE)
            if session:
                return session.access_token
        except Exception as e:
            print(e)
            raise Exception(str(e))
