# File for calling AWS resources (AWS Lambda and API Gateway)
from urllib import request, parse
from urllib.error import HTTPError
from fastapi import HTTPException
from settings import AWS_API_GATEWAY_KEY

class AWSClient:
    """
    Class description
    """

    def __init__(self):
        """
        Constructor method
        """
        pass

    def call_lambda_function(self, lambda_uri: str, token: str) -> str:
        """
        Chama função do AWS lambda e retorna o resultado

        :param lambda_uri: URL da função no AWS Lambda/API Gateway
        :type lambda_uri: str
        :param token: Token JWT com as permissões de execução da função
        :type token: str
        :returns str: Resposta da chamada de função
        :raises HttpException: HttpException se a função retornou um código HTTP de erro 
        """

        try:
            req = request.Request(lambda_uri)
            req.add_header('authorizationToken', token)

            with request.urlopen(req) as response:
                response_data = response.read().decode('utf-8')
                if response_data:
                    return response_data
        except HTTPError as error:
            raise HTTPException(status_code=error.code)
        except Exception as e:
            logging.debug(e)
            raise HTTPException(status_code=403)
