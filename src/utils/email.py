# Módulo de envio de emails utilizando o servidor SMTP do Gmail

import smtplib, ssl
from settings import KEYCLOAK_BASE_URL, EMAIL_ADDRESS, EMAIL_PASSWORD, KEYCLOAK_DEFAULT_USER_PASSWORD

def get_reset_password_message(full_name: str, username: str, realm: str) -> str:
    """
    Function description

    :param1 param1: First parameter
    :type param1: Type of parameter
    :returns: Description of what is returned
    :raises keyError: Description of error raised
    """

    message = f"""\
    Subject: Keycloak reset password message

    Hello {full_name}. Please click the link below to reset your password:

    {KEYCLOAK_BASE_URL}/auth/realms/{realm}/account
    Username: {username}
    Temorary password: {KEYCLOAK_DEFAULT_USER_PASSWORD}

    You will be prompted for password change on login.
    This is an automated message, do not reply.
    """

    return message

def send_email(message: str, recipient_email: str) ->  bool:
    """
    Envia um email de mudança de senha ao usuário

    :param1 message: Mensagem a ser enviada
    :type param2: str
    :param2 recipient_email: Email do usuário
    :type param2: str
    :returns bool: Se o email for enviado com sucesso, retorna verdadeiro
    :raises Exception: Lança exceção se ocorrer algo no envio
    """

    port = 465  # ssl
    address = EMAIL_ADDRESS
    password = EMAIL_PASSWORD

    try:
        # Create context
        context = ssl.create_default_context()

        # Send email
        with smtplib.SMTP_SSL('smtp.gmail.com', port, context=context) as server:
            server.login(address, password)
            server.sendmail(address, recipient_email, message)
            return True
    except Exception as e:
        raise Exception(str(e))


def send_update_password_email(full_name: str, username: str, realm: str, recipient_email: str) -> bool:
    
    """
    Envia o email de alteração de senha ao usuário
    :param1 fullname: Nome completo do usuário
    :type param1: str
    :param2 username: Nome de usuário
    :type param2: str
    :param3 realm: Nome do realm
    :type param3: str
    :param4 recipient_email: Email do usuário
    :type param4: str
    :returns bool: Verdadeiro se o email foi enviado corretamente
    :raises Exception: Se houver erro no envio de email lança exceção
    """

    # Get email message
    message = get_reset_password_message(full_name, username, realm)

    # Send message to recipient
    response = send_email(message, recipient_email)
    if response:
        return True
    else:
        raise Exception("Could not send update password email")

