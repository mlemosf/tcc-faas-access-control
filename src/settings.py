# Arquivo de configurações

from decouple import config

# Keycloak credentials
KEYCLOAK_API_SECRET = config('KEYCLOAK_API_SECRET')
KEYCLOAK_DEFAULT_USER_PASSWORD = config('KEYCLOAK_DEFAULT_USER_PASSWORD', default='asdf')
KEYCLOAK_ADMIN_USERNAME = config('KEYCLOAK_ADMIN_USERNAME')
KEYCLOAK_ADMIN_PASSWORD = config('KEYCLOAK_ADMIN_PASSWORD')
KEYCLOAK_BASE_URL = config('KEYCLOAK_BASE_URL')
REALM_CLIENT_ID = config('REALM_CLIENT_ID')
REALM_CLIENT_SECRET = config('REALM_CLIENT_SECRET')
DEBUG_USER_USERNAME = config('DEBUG_USER_USERNAME')
DEBUG_USER_PASSWORD = config('DEBUG_USER_PASSWORD')

# Email credentials
EMAIL_PASSWORD = config('EMAIL_PASSWORD')
EMAIL_ADDRESS = config('EMAIL_ADDRESS')

# AWS API Gateway credentials
AWS_API_GATEWAY_KEY = config('AWS_API_GATEWAY_KEY')

# Variável booleana para verificar validade do certificado para TLS (recomendável deixar como 'True' em ambientes de produção)
TLS_VERIFY_CERTIFICATE = config('TLS_VERIFY_CERTIFICATE', cast=bool, default=True)

# Variáveis da aplicação
API_BASE_URL = config('API_BASE_URL')
