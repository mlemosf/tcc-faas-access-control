# Arquivo de rotas da API
# Rotas a fazer
# GET /health - health check - OK
# POST /{realm}/user/new - Cria o usuário OK
# GET /{realm}/authorization/get - Gera a URL de autenticação no Keycloak - OK
# GET /{realm}/authorization/token - Gera um token JWT após login do usuário - OK
# POST /{realm}/function/exec - Lista as funções existentes - OK

from typing import Optional
from fastapi import FastAPI, Header, HTTPException, Response
from fastapi.responses import RedirectResponse
from fastapi.middleware.cors import CORSMiddleware
from datetime import datetime
from pydantic import BaseModel
from keycloak.keycloak import Keycloak
from providers.aws import AWSClient
from utils import email
from settings import API_BASE_URL, KEYCLOAK_BASE_URL

class User(BaseModel):
    """
    Classe de usuário do sistema
    """

    username: str
    email: str
    first_name: Optional[str] = None
    last_name: Optional[str] = None


class Function(BaseModel):
    """
    Classe de template de função
    """

    function_url: str

app = FastAPI()

# Origens permitidas pelo CORS
origins = [
    API_BASE_URL
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/health")
def read_root():
    return {"message": "OK", "timestamp": datetime.now()}


@app.post("/{realm}/user/new")
async def create_user(realm: str, user: User) -> dict:
    """
    Cria um novo usuário no Keycloak

    :param user:  Usuário
    :type user: User
    :param realm:  Nome de um realm do keycloak
    :type realm: string
    :raises Exception: Quaisquer erros lançados pelas funções internas
    :returns dict: Dicionário com mensagem de sucesso/fracasso
    """

    try:
        keycloak = Keycloak(keycloak_endpoint=KEYCLOAK_BASE_URL)
        response = keycloak.create_user(user, realm)
        full_name = f'{user.first_name} {user.last_name}'
        email.send_update_password_email(full_name, user.username, realm, user.email)
        response = True
        message = {
            'message': response
        }
    except Exception as e:
        message = {
            'message': str(e)
        }
    finally:
        return message


@app.get('/{realm}/authorization/get')
async def get_authorization_url(realm: str) -> str:
    """
    Gera uma URL de autenticação com o Keycloak

    :param realm: Nome do realm
    :type param: str
    :returns RedrectResponse: URL de autenticação
    :raises Exception: Qualquer erro lançado pelas funções internas
    """

    try:
        keycloak = Keycloak(keycloak_endpoint=KEYCLOAK_BASE_URL, realm=realm)

        # URL de autorização
        redirect_uri = f'{API_BASE_URL}/{realm}/authorization/token'
        authorization_url = keycloak.generate_oauth2_authorization_url(realm, redirect_uri=redirect_uri)
        message = {
            'message': authorization_url
        }
    except Exception as e:
        message = {
            'message': str(e)
        }
    finally:
        return RedirectResponse(message['message'])


@app.get('/{realm}/authorization/debug')
async def get_authorization_url_automatic(realm: str) -> str:
    """
    Retorna o token de acesso de forma automatizada para fins de teste
    :param realm: Nome do reakm
    :type realm: str
    :returns: str
    """

    try:
        keycloak = Keycloak(keycloak_endpoint=KEYCLOAK_BASE_URL, realm=realm)

        # URL de autorização
        redirect_uri = f'{API_BASE_URL}/{realm}/authorization/token'
        response = keycloak.generate_automatic_oauth2_bearer_token(realm=realm)
        bearer_token = response
        message = {
            'message': bearer_token
        }
    except Exception as e:
        message = {
            'message': str(e)
        }
    finally:
        return message



@app.get('/{realm}/authorization/token')
async def get_bearer_token(realm: str, code: str) -> str:
    """
    Retorna um token de acesso para o lambda

    :param realm: Nome do realm
    :type realm: str
    :param code: Código de autenticação
    :type code: str
    :returns: str
    :raises Exception: Retorna quaisquer erros as funções internas retornem
    """

    try:
        keycloak = Keycloak(keycloak_endpoint=KEYCLOAK_BASE_URL, realm=realm)
        authentication_code = code

        # Autentica o usuário e gera um token de autorização
        redirect_uri = f'{API_BASE_URL}/{realm}/authorization/token'
        token = keycloak.get_token(realm, authentication_code, redirect_uri=redirect_uri)

        message = {
            'message': f'Bearer {token}'
        }
    except Exception as e:
        message = {
            'message': str(e)
        }
    finally:
        return message

@app.post('/{realm}/function/exec')
async def call_function(realm: str, function: Function,response: Response, authorization: str = Header(None)) -> dict:
    """
    Chama função em ambiente de FaaS do provedor

    :param realm: Nome do realm
    :type realm: str
    :param function: Corpo da chamada (url da função)
    :type function: Function
    :param authorization: Header 'Authorization' que recebe o Bearer token
    :type authorization: str
    :returns dict: dicionário com código de resposta do provedor e corpo da resposta
    :raises HttpException: Lança erro 403 (forbidden) caso o token de acesso não seja passado
    """

    try:
        if not authorization:
            raise HTTPException(status_code=403, detail='No bearer token found')

        aws = AWSClient()
        response = aws.call_lambda_function(function.function_url, authorization)
        message = {
            'message': {
                'provider_response_code': 200,
                'provider_response': response
            }
        }
        return message
    except HTTPException as e:
        message = {
            'message': {
              'provider_response_code': e.status_code,
              'provider_response': e.detail
            }
        }
        response.status_code = 400 # Código de erro geral para erros do provedor 
        return message
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
